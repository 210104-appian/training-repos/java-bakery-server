const baseUrl = "http://localhost:8082/java-bakery-server";

const itemsUrl = baseUrl + "/items";
const breadUrl = itemsUrl + "?type=bread";
const muffinUrl = itemsUrl + "?type=muffin";

/*
performAjaxGetRequest(breadUrl, function(json){
    let bread = JSON.parse(json);
    console.log(bread);
})
*/


function performAjaxGetRequest(url, callback){
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4){
            callback(xhr.response); // this is going to be the response body of our http response  (JSON)
        }
    }
    xhr.send();
}

function getAllBread(callback){
    performAjaxGetRequest(breadUrl, callback);
}

function getAllMuffins(callback){
    performAjaxGetRequest(muffinUrl, callback);
}