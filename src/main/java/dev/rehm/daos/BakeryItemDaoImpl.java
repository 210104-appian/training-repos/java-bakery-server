package dev.rehm.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dev.rehm.models.BakeryItem;
import dev.rehm.models.Bread;
import dev.rehm.models.Coffee;
import dev.rehm.models.Muffin;
import dev.rehm.models.Size;
import dev.rehm.util.ConnectionUtil;

public class BakeryItemDaoImpl implements BakeryItemDao {

	@Override
	public List<BakeryItem> getAllBakeryItems() {
		try (Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement();){
			
			ResultSet resultSet = statement.executeQuery("SELECT * FROM BAKERY_ITEM"); // because we don't have any parameters, a normal statement is safe here
			return processBakeryItemResultSet(resultSet);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return null;
	}

	@Override
	public List<String> getAllBakeryItemTypes() {
		String sql = "SELECT ITEM_TYPE FROM BAKERY_ITEM GROUP BY ITEM_TYPE";
		List<String> types = new ArrayList<>();
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement();){
			ResultSet rs = statement.executeQuery(sql);
			while(rs.next()) {
				types.add(rs.getString("ITEM_TYPE"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return types;
	}

	@Override
	public List<BakeryItem> getBakeryItemsByType(String type) {
		String sql = "SELECT * FROM BAKERY_ITEM WHERE ITEM_TYPE = ?";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)){
			pStatement.setString(1, type);
			ResultSet rs = pStatement.executeQuery();
			return processBakeryItemResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public BakeryItem addNewBakeryItem(BakeryItem item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean changeItemPrice(int itemId, double newPrice) {
		String sql = "UPDATE BAKERY_ITEM SET PRICE = ? WHERE ID = ?";
		int numOfItemsUpdated = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)){
			pStatement.setDouble(1, newPrice); // 1 is the 1st '?'
			pStatement.setInt(2, itemId); // 2 is the 2nd '?'
			numOfItemsUpdated = pStatement.executeUpdate(); // this returns number of rows affected
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(numOfItemsUpdated==1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean removeItemById(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	
	private List<BakeryItem> processBakeryItemResultSet(ResultSet resultSet) throws SQLException{
		List<BakeryItem> items = new ArrayList<>();
		while(resultSet.next()) {
			String itemType = resultSet.getString("ITEM_TYPE");

			switch(itemType) {
			case "MUFFIN":
				Muffin muffin = new Muffin();
				muffin.setId(resultSet.getInt("ID"));
				muffin.setPrice(resultSet.getDouble("PRICE"));
				muffin.setFlavor(resultSet.getString("MUFFIN_FLAVOR"));
				items.add(muffin);
				break;
			case "BREAD":
				Bread bread = new Bread();
				bread.setId(resultSet.getInt("ID"));
				bread.setPrice(resultSet.getDouble("PRICE"));
				bread.setType(resultSet.getString("BREAD_TYPE"));
				items.add(bread);
				break;
			case "COFFEE":
				Coffee coffee = new Coffee();
				coffee.setId(resultSet.getInt("ID"));
				coffee.setPrice(resultSet.getDouble("PRICE"));
				coffee.setRoast(resultSet.getString("COFFEE_ROAST"));
				String coffeeSize = resultSet.getString("COFFEE_SIZE");
				if(coffeeSize!=null) {
					Size size = Size.valueOf(coffeeSize);
					coffee.setSize(size);
				}
				items.add(coffee);
				break;
			}
		}
			return items;
	}
}
