package dev.rehm.daos;

import java.util.List;

import dev.rehm.models.Order;

public interface OrderDao {
	
	public List<Order> getAllOrders();
	public Order createNewOrder(Order order);

}
