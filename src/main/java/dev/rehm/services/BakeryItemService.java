package dev.rehm.services;

import java.util.List;

import dev.rehm.daos.BakeryItemDao;
import dev.rehm.daos.BakeryItemDaoImpl;
import dev.rehm.models.BakeryItem;

public class BakeryItemService {
	
	private BakeryItemDao itemDao = new BakeryItemDaoImpl();
	
	public List<BakeryItem> getAllItems(){
		return itemDao.getAllBakeryItems();
	}
	
	public List<BakeryItem> getItemsByType(String type){
		return itemDao.getBakeryItemsByType(type);
	}

}
