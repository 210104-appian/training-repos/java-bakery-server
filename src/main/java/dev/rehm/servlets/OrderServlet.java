package dev.rehm.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// make sure to register your class in the web.xml
public class OrderServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// order + order items will be in request body, so we need to obtain the request body from the HttpServletRequest
		
		// use the ObjectMapper to convert string -> java object 
		
		// send java object to service which will pass that object to the DAO and persist to our DB 
		
		// configure HttpServletResponse with appropriate status (201 - created), possibly 400/500 depending on what kind of issue
	}
	
}
