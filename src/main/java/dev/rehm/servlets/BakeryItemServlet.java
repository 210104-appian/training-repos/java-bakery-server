package dev.rehm.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.rehm.models.BakeryItem;
import dev.rehm.models.Bread;
import dev.rehm.models.Coffee;
import dev.rehm.models.Muffin;
import dev.rehm.services.BakeryItemService;

public class BakeryItemServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private BakeryItemService itemService = new BakeryItemService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("GET request to Bakery Item Servlet");
		String itemType = request.getParameter("type");
		System.out.println("requesting type: "+itemType);
		
		List<BakeryItem> items;
		if(itemType!=null) {
			items = itemService.getItemsByType(itemType.toUpperCase());
		} else {
			// item type is not provided, return all of the item data to client
			items = itemService.getAllItems();
	//		System.out.println(items);
		}
		
		ObjectMapper om = new ObjectMapper();
		String itemsJson = om.writeValueAsString(items);
//		System.out.println(itemsJson);
		PrintWriter pw = response.getWriter();
		pw.write(itemsJson);
		pw.close();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

	}
	
}
